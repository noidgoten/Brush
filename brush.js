// used to smoothen the switch from one color to another
// there's PAINTERS / BULK painters and one bulk contains BULK mini-painters
// used to switch from one color to another
function convexPath(a, b, alpha){
    return (1 -alpha) * a + alpha * b;
}

// main and only brush
function Ribbon(context) {
    this.init(context);
}

Ribbon.prototype =
{
    context: null,
    mouseX: null, mouseY: null,
    painters: null,
    interval: null,
    // flag set to false when no need to process the animation loop
    animate: false,

    init: function (context) {
        this.context = context;
        // draw over existing stuff
        this.context.globalCompositeOperation = 'source-over'; // default behavior
        this.mouseX = undefined;
        this.mouseY = undefined;
        this.painters = new Array();
        // initialization
        for (var i = 0; i < PAINTERS; i++) {
            this.painters.push({
                dx: undefined,
                dy: undefined,
                ax: 0,
                ay: 0,
                div: 0.04,
                ease: 0.5 + i / 10 / PAINTERS,
                color: undefined
            });
        }
        this.generateColors(this);
        // good idea, 'this' will no longer be the ribbon in the setinterval
        var scope = this;
        // run the animation loop
        setInterval(function() {
            if(scope.animate){
                scope.context.lineWidth = BRUSH_SIZE;
                for (var i = 0; i < scope.painters.length; i++) {
                    scope.context.strokeStyle = scope.painters[i].color;
                    scope.context.beginPath();
                    scope.context.moveTo(scope.painters[i].dx, scope.painters[i].dy);
                    scope.painters[i].dx -= scope.painters[i].ax = (scope.painters[i].ax + (scope.painters[i].dx - scope.mouseX) * scope.painters[i].div) * scope.painters[i].ease;
                    scope.painters[i].dy -= scope.painters[i].ay = (scope.painters[i].ay + (scope.painters[i].dy - scope.mouseY) * scope.painters[i].div) * scope.painters[i].ease;
                    scope.context.lineTo(scope.painters[i].dx, scope.painters[i].dy);
                    scope.context.stroke();
                }
            }
        }, 1000 / FPS);
    },

    // generates colors for the painters
    generateColors: function(scope){
        var r = [],
            g = [],
            b = [];
        for(var i = 0; i < Math.ceil(PAINTERS / BULK) + 1; ++i) {
            r.push(Math.floor(Math.random() * 256));
            g.push(Math.floor(Math.random() * 256));
            b.push(Math.floor(Math.random() * 256));
        }
        var ai, bi, alpha;
        for(var i = 0; i < PAINTERS; ++i) {
            ai = Math.floor(i / BULK);
            bi = Math.floor(i / BULK + 1);
            alpha = (i % BULK) / BULK;
            scope.painters[i].color = "rgba(" + convexPath(r[ai], r[bi], alpha) + ", "
                + convexPath(g[ai], g[bi], alpha) + ", "
                + convexPath(b[ai], b[bi], alpha) + ", 0.15)";
        }
    },

    strokeStart: function (mouseX, mouseY) {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
        for (var i = 0; i < this.painters.length; i++) {
            this.painters[i].dx = mouseX;
            this.painters[i].dy = mouseY;
        }
        this.animate = true
    },

    stroke: function (mouseX, mouseY) {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    },

    strokeEnd: function () {
        this.animate = false;
    }
};

var BRUSHES = ["Ribbon"],
    BRUSH_SIZE = 2,
    PAINTERS = 50,
    BULK = 10,
    FPS = 144,
    brush,
    mouseX = 0,
    mouseY = 0,
    canvas,
    context;

(function () {
    var hash;
    document.body.style.backgroundRepeat = 'no-repeat';
    document.body.style.backgroundPosition = 'center center';
    canvas = document.createElement("canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    canvas.style.cursor = 'crosshair';
    document.body.appendChild(canvas);
    context = canvas.getContext("2d");
    // www.domain.com/lal#hashtag then w.l.h is #hashtag
    // useless for nao
    if (window.location.hash) {
        hash = window.location.hash.substr(1, window.location.hash.length);
        for (var i = 0; i < BRUSHES.length; i++) {
            if (hash == BRUSHES[i]) {
                brush = eval("new " + BRUSHES[i] + "(context)");
                menu.selector.selectedIndex = i;
                break;
            }
        }
    }
    if (!brush) {
        brush = eval("new " + BRUSHES[0] + "(context)");
    }
    window.addEventListener('mousemove', onWindowMouseMove, false);
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('keypress', onkeydown);
    canvas.addEventListener('mousedown', onCanvasMouseDown, false);
    canvas.addEventListener('touchstart', onCanvasTouchStart, false);
    ["60", "120", "144"].forEach(function(string) {
        document.getElementById(string).addEventListener('click', fps_selector);
    });
})();

function fps_selector(event){
    FPS = parseInt(event.target.id);
    var button;
    ["60", "120", "144"].forEach(function(string){
        button = document.getElementById(string);
        button.className = button.className.replace(" active", "");
    });
    event.target.className += " active";
}

// WINDOW
function onWindowMouseMove(event) {
    mouseX = event.clientX;
    mouseY = event.clientY;
}

function onWindowResize() {
    // deletes everything on resize (best solution i could come up with)
    context.clearRect(0, 0, canvas.width, canvas.height);
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}

// DOCUMENT
function onDocumentMouseDown(event) {
    event.preventDefault();
}

// CANVAS
function onCanvasMouseDown(event) {
    brush.strokeStart(event.clientX, event.clientY);
    window.addEventListener('mousemove', onCanvasMouseMove, false);
    window.addEventListener('mouseup', onCanvasMouseUp, false);
}

function onCanvasMouseMove(event) {
    brush.stroke(event.clientX, event.clientY);
}

function onCanvasMouseUp() {
    brush.strokeEnd();
    window.removeEventListener('mousemove', onCanvasMouseMove, false);
    window.removeEventListener('mouseup', onCanvasMouseUp, false);
}

function onkeydown(event){
    // change color key
    if(event.code == "KeyC"){
        brush.generateColors(brush);
    }
    // reset canvas key
    if(event.code == "KeyR"){
        context.clearRect(0, 0, canvas.width, canvas.height);
    }
}

// still to test
function onCanvasTouchStart(event) {
    if (event.touches.length == 1) {
        event.preventDefault();

        brush.strokeStart(event.touches[0].pageX, event.touches[0].pageY);

        window.addEventListener('touchmove', onCanvasTouchMove, false);
        window.addEventListener('touchend', onCanvasTouchEnd, false);
    }
}

function onCanvasTouchMove(event) {
    if (event.touches.length == 1) {
        event.preventDefault();
        brush.stroke(event.touches[0].pageX, event.touches[0].pageY);
    }
}

function onCanvasTouchEnd(event) {
    if (event.touches.length == 0) {
        event.preventDefault();

        brush.strokeEnd();

        window.removeEventListener('touchmove', onCanvasTouchMove, false);
        window.removeEventListener('touchend', onCanvasTouchEnd, false);
    }
}